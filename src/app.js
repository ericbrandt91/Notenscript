"use strict";
import querystring from 'querystring';
import fetch from 'node-fetch';
import Promise from 'promise';
import bodyParser from 'body-parser';
import telebot from 'node-telegram-bot-api';
import Note from './notenAPI/Note';
import Noten from './notenAPI/Noten';
import TestNoten from './test/testdata';
import {ChatID,BotToken,Zugang} from './config/config';

let grades = new Noten();
//DEBUG: console.log(Zugang);

//Init App
grades.fetchExams(Zugang)
.then((result) => sendTelegramMessage("NotenBot initialisiert, die letzte Note war: " + result['PrTxt']))
.then(() => startRoutine())
.catch((err) => {console.log(err)});


var startRoutine = () => {
    setInterval(genHeapStats,10000);
    setInterval(checkForGrades,10000);
}
  
let checkForGrades = ()  => {
    grades.fetchExams(Zugang)
    .then((result) => {
            if(result)sendTelegramMessage("Neue Note ist draußen: "+ result['PrTxt']);
            else{console.log("Nix neues")}
        }
    )
    .catch((err) => {console.log(err)});
}  

//Für Telegram
let sendTelegramMessage = (msg) => {
    console.log("ID"+ChatID+" BotTOken" + BotToken );   
    let bot = new telebot(BotToken, {polling:true});
    bot.sendMessage(ChatID,msg);
}


//Stackoverflow auf dem Raspberry vermeiden durch manuelles leeren des GC
let genHeapStats = () => {
	try{
	    global.gc();
	}catch(e){
	    console.log("you must run program with 'node --expose-gc'");
	}
	//var heapUsed = process.memoryUsage().heapUsed;
	//console.log("Program is using " + heapUsed +" bytes of Heap");
	//process.kill(process.pid, 'SIGUSR2');
}


