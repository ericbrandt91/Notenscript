import querystring from 'querystring';
import fetch from 'node-fetch';
import Promise from 'promise';
class Noten {

    constructor(){
        this.Noten = {}
    }

    addExam(Note){
        let {PrNr,PrTxt} = Note;
        if(!this.Noten[PrNr]){
            this.Noten[PrNr] = Note;
            return Note;
        }else {
            return false;
        }
    }
    
    getExams(){
        return this.Noten;
    }

    fetchExams(Zugang){
       //DEBUG: console.log("Promise called" + JSON.stringify(Zugang));
        
       return new Promise((resolve,reject) => {
            fetch("https://wwwqis.htw-dresden.de/appservice/getgrades", {
                method: "POST",
                body: querystring.stringify(Zugang),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).then((result) => {return result.json();})
            .then((res) => {
                let result = false;
                            res.map((Note)=>{
                                if(this.addExam(Note)){
                                   result = Note; 
                                }
                            });                
                resolve(result);
                          
                            
            })
            .catch((err) => {reject(err);});
      });
    }

    

}

export default Noten;