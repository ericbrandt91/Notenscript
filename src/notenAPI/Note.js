class Note {

    constructor(Note){
      let {PrNr,PrTxt,PrDatum} = Note;
      this.PrNr = PrNr;    
      this.PrTxt = PrTxt;
      this.PrDatum = PrDatum;               
    }

}

export default Note;

