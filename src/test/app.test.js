
import Noten from '../notenAPI/Noten';


var assert = require('assert');
var grades = new Noten();
var testData =  [{
        "PrNr": "5080",
        "Status": "BE",
        "EctsCredits": "5.0",
        "PrTxt": "Progr. v. Komp.",
        "Semester": "20161",
        "Versuch": "1",
        "PrDatum": "20.07.2016",
        "PrNote": "100",
        "VoDatum": "13.09.2016",
        "PrForm": "S",
        "Vermerk": "",
        "EctsGrade": ""
    }];


describe('Noten',function(){
    describe('#addExam',function(){
        it('should return the added Exam when grade is not within the map',function(){
            assert.equal(testData[0],grades.addExam(testData[0]));
        })
    })
})

describe('Noten',function(){
    describe('#addExam',function(){
        it('should return false when grade is already within the map',function(){
            assert.equal(false,grades.addExam(testData[0]));
        })
    })
})


