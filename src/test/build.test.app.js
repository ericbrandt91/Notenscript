'use strict';

var _Noten = require('../notenAPI/Noten');

var _Noten2 = _interopRequireDefault(_Noten);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

assert = require('assert');
var grades = new _Noten2.default();

describe('Array', function () {
    describe('#indexOf()', function () {
        it('should return -1 when the value is not present', function () {
            assert.equal(-1, [1, 2, 3].indexOf(4));
        });
    });
});
