# Intention
This project was written to notify myself and several students of my course whenever a new grade arrives in the HISQIS system of our university. Thus we leverage the subscripe/push mechanism of the Telegram API.

# Usage
## clone this repository 
  
> git clone https://gitlab.com/ericbrandt91/Notenscript.git

## install dependencies

I presume that you already have installed NodeJS on your system, otherwise follow this instructions: https://nodejs.org/en/download/package-manager/

switch into the folder you've downloaded a few moments ago and type the following command.

> npm install 

## adjusting options

adjust the sNummer and the RZLogin field to your own needs.
The StgNr, AbschlNr, POVersion could be retreived on this website: https://www2.htw-dresden.de/~app/API/

## Create a Telegram Bot 
Telegram is an open source instant messenger which provides you to invite automated bots in a chat or group channel. In this example the bot will tell you, when new grades arrived.
To create a bot we recommend this documentation: https://core.telegram.org/api
<code>
let config = {
    'ChatID': '<Telegram ChatID in which the Bot is located>',
    'BotToken':'<Token of your Telegram Bot>',
    'Zugang':{
        "sNummer": "<your bib number>",
        "RZLogin": "<your unix password>",
        "AbschlNr":"84",
        "StgNr":"277",
        "POVersion":"2010"
    }
}

</code>

## build the app and run it

> npm start

## running tests

> npm run test

