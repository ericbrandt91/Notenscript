module.exports = {
    entry: {
        noten: './src/test/app.test.js',
        app:'./src/test/app.test.js'
    },
    output: {
        filename: '[name].test.build.js',
        path: './build/tests/',
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                loaders: ['babel-loader']
            }]
    }
};